-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

My personal PGP public key follows. This key is also available at https://www.doctormckay.com and https://alexandercorn.com, and on MIT's key server at http://pgp.mit.edu/pks/lookup?op=get&search=0xB3BF12A31A7EA4A1.

This key was generated on April 22, 2014 and has no expiration date. It is valid for Alexander Corn at the email address alex@alexandercorn.com. The key ID is 1A7EA4A1 and the fingerprint is 3607536C161B0CF7F187B2E1B3BF12A31A7EA4A1.

In the unlikely event that I must revoke this key, a revocation certificate will be uploaded to MIT's key server.

This document was written and signed on July 7, 2014 and is available at https://github.com/DoctorMcKay/pgp and https://bitbucket.org/Doctor_McKay/pgp.
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.22 (MingW32)

iQEcBAEBAgAGBQJTuuEIAAoJELO/EqMafqShQfoH/A2ibDmm3v9hZy3TmEUfwVpe
1j2kW59KtT+bhriMEx0OgX1rjxJizu7mdcwrsJRJeu/3gC+9GwWnxVnYcBSXpWBO
WnlGXkZlP+FnPM9Mhzm0J7xJ+pAO1NBGIVU7yGVZTME+nldi94sbpwWwMhdD67O5
aJKDY/9M8Pf5kN6KDtLuwRUQjeQ8OXKa4VKfJShmamT8pWFezjd9VhLTWQ060vHm
dEVvtX9ozgc896cLtsENeIv/lWwYDFZYdwa1oHJyNttUYlU1vTKfva1Hg95lovXb
dx1Op8P/W1yGeBtq6MpkcmdhDbCMicoVrvvLEel8H22qR+Ljq74YZQRe7pRJmnc=
=s824
-----END PGP SIGNATURE-----


-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2.0.22 (MingW32)

mQENBFNXLG8BCADaN+zqPoiNNQ+2iuHGGbVwuCxPnVif2jBCZeiSS1cO/SbBUv5C
41gsSujh2DRL9IZIcOeiR08/PEBH7GJBedh10rPU0A1hmBc5DFuVRBsoKZ4bu+9Y
zt/nxu4WhhTpW2T2hTyGPf10XIu40k0SHinnNHBDS+bzxm5DsloWmAxuWciiPKVR
0C7oSUbvxIPN+o6kXT0adfBDQrydaF6fcFb4dbp4CvJ2zybi2wGxmdkmDP02LYyQ
STgXWcHNsBX710UodSbpSLe8+OLAucyq2rZQVZDYOSGNdlm85roimFgNlVlNjq2x
rZY7L3HMTZkbYHtr/RvqCyBjVptzNdYYYLZzABEBAAG0J0FsZXhhbmRlciBDb3Ju
IDxhbGV4QGFsZXhhbmRlcmNvcm4uY29tPokBOQQTAQIAIwUCU1csbwIbDwcLCQgH
AwIBBhUIAgkKCwQWAgMBAh4BAheAAAoJELO/EqMafqShjuYIAK4MKsspfQKFkPXU
CzB+2+WQhfGeronShSAeGYpvp+ZjQ5iSOP8/YTyY7xGKz8emaOyIPF85DUmP+E9a
eCFNZBBqf2ePhEgTZjMB1t83Pn5dRAUM6kZt6hGzQn4R5RQH8V/YG6RCbPI42sMR
fhIHmgErpW8MQU7wvkZLwjYi9t0Gqn3P4fxNVWjZBgkH5OfoDaI7Y3HxqtRTNdhA
WclgomJ8Yjb2L7GuuFtSDSAlRW1PfHD0xBrAB85Eh7qf0KblFd4h05ZlFZFf0rF8
Pyrs+yXaaW0InzQQQAKjeGgvjTaAJbuCTuZtDvx6eCsvJZ42OZj5eK1eYLYZ2qIt
oOLBlcA=
=MQbB
-----END PGP PUBLIC KEY BLOCK-----